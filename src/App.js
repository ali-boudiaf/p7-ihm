import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Erreur from "./structure/Erreur";
import Waiting from "./structure/Waiting";
import Keycloak from "keycloak-js";
import { bindActionCreators } from "redux";
import { setKeycloak, addErreur } from "./reducers/generalReducer";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";
import { blue, orange } from "@material-ui/core/colors";
import getConfiguration from "./utils/configuration";
import Accueil from "./pages/Accueil";
// import Lot from "./pages/Lot";

const defaultTheme = createMuiTheme({
  overrides: {
    MuiSelect: {
      select: {
        minWidth: 80
      }
    }
  },
  palette: {
    primary: { main: blue.A700 },
    secondary: { main: orange[200] }
  },
  typography: {
    useNextVariants: true,
    button: {
      textTransform: "none"
    }
  }
});

class App extends React.Component {
  // A commenter pour un devà la maison
  componentDidMount() {
    const { setKeycloak, addErreur } = this.props;
    getConfiguration("keycloakFile").then(res => {
      const keycloak = Keycloak(`/${res}`);
      keycloak
        .init({ onLoad: "login-required" })
        .success(() => {
          setKeycloak(keycloak);
        })
        .error(erreur => {
          addErreur(erreur, "Echec de l'authentification Keycloak");
        });
    });
  }

  render() {
    // A commenter pour un dev à la maison
    const { keycloak } = this.props;
    if (!keycloak) {
      return "Vous devez être authentifié pour accéder à l'application";
    }
    return (
      <MuiThemeProvider theme={defaultTheme}>
        <div>
          {/* <Header /> */}
          <Erreur />
          <Waiting />
          <Switch>
            {/* <Route path="/accueil" component={Accueil} /> */}
            <Route path="/" component={Accueil} />
            {/* <Route path="/lot" component={Lot} /> */}
          </Switch>
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = ({ generalReducer }) => {
  return {
    keycloak: generalReducer.keycloak
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators({ setKeycloak, addErreur }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
