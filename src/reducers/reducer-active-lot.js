import { AT_LOTS } from "../actions/action-types";

export default function reducerActiveLot(state = null, action) {
  switch (action.type) {
    case AT_LOTS.READ:
      return action.payload;
  }

  return state;
}
