import { AT_LOTS } from "../actions/action-types";

export default function reducerLots(state = [], action) {
  switch (action.type) {
    case AT_LOTS.READ_ALL:
      return action.payload;
  }
  return state;
}
