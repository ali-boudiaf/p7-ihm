import React, { Component } from "react";
import axios from "axios";

const API_END_PONT = "http://localhost:9090/";

const LOTS_REPRISE_Q = "lots-reprise-q/idep/toto";

class TestAPI extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    axios.get(`${API_END_PONT}${LOTS_REPRISE_Q}`).then(function(response) {
      console.log("-------------");
      console.log("", response);
      console.log("-------------");
    });
  }

  render() {
    return <div></div>;
  }
}

export default TestAPI;
