import store from "index";
import axios from "axios";
import getConfiguration from "./configuration";

export const refreshToken = (kc, minValidity = 5) => {
  return new Promise((resolve, reject) => {
    kc.updateToken(minValidity)
      .success(() => resolve())
      .error(error => reject(error));
  });
};

export async function API() {
  const nomServeur = await getConfiguration("api");
  const myAxios = axios.create({
    baseURL: `${nomServeur}`
  });

  const kc = store.getState().generalReducer.keycloak;

  myAxios.interceptors.request.use(config => {
    return refreshToken(kc)
      .then(() => {
        config.headers.Authorization = `Bearer ${kc.token}`;
        config.headers["Content-Type"] = "application/json;charset=utf-8";
        config.headers.Accept = "application/json;charset=utf-8";
        return Promise.resolve(config);
      })
      .catch(() => {
        kc.login();
      });
  });

  return myAxios;
}
