const initialState = {
  waiting: 0,
  context: null,
  erreurs: [],
  keycloak: null,
  snack: ""
};

export const generalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "WAITING_TRUE": {
      return Object.assign({}, state, {
        waiting: state.waiting + 1
      });
    }
    case "WAITING_FALSE": {
      return Object.assign({}, state, {
        waiting: state.waiting - 1
      });
    }
    case "ADD_ERREUR": {
      const newErreurs = state.erreurs.slice();
      newErreurs.push({ error: String(action.error), log: action.log });
      return Object.assign({}, state, {
        erreurs: newErreurs,
        waiting: state.waiting - 1
      });
    }
    case "CLEAR_ERREUR": {
      return Object.assign({}, state, {
        erreurs: []
      });
    }
    case "SET_CONTEXT": {
      return Object.assign({}, state, {
        context: action.context
      });
    }
    case "SET_KEYCLOAK": {
      return Object.assign({}, state, {
        keycloak: action.keycloak
      });
    }
    case "SET_SNACK": {
      return Object.assign({}, state, { snack: action.snack });
    }
    default:
      return state;
  }
};

export const setSnack = snack => {
  return { type: "SET_SNACK", snack };
};

export const setKeycloak = keycloak => {
  return {
    type: "SET_KEYCLOAK",
    keycloak
  };
};

export const waitingFalse = () => {
  return {
    type: "WAITING_FALSE"
  };
};

export const clearErreur = () => {
  return {
    type: "CLEAR_ERREUR"
  };
};

export const addErreur = (error, log) => {
  return {
    type: "ADD_ERREUR",
    error,
    log
  };
};

export const setContext = context => {
  return {
    type: "SET_CONTEXT",
    context
  };
};

export const waitingTrue = () => {
  return {
    type: "WAITING_TRUE"
  };
};
