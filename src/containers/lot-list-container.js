import { connect } from "react-redux";
import React, { useState, useEffect } from "react";
import LotList from "../components/lot-list";
import axios from "axios";

const END_POINT = "http://localhost:9090/";
const LOTS_REPRISE_Q = "lots-reprise-q/idep/";
const LOTS_REPRISE_Q_PRENDRE = "lots-reprise-q/nonaffecte/";

const LotListContainer = props => {
  const [lots, setLots] = useState([]);
  const { idep } = props;

  console.log("value lots: ", lots);
  useEffect(() => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("récupérer lots", response);
        console.log("-------------");
        setLots(response.data);
      })
      .catch(error => {});
  }, []);
  useEffect(() => console.log("changement"), [lots]);

  function ajoutLotOnClick() {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_PRENDRE}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("ajouter un lot", response);
        console.log("-------------");

        console.log("ajouter un lot (lots) B", lots);
        const nouveauLots = [...lots, response.data];
        console.log("ajouter un lot (lots) A", lots);
        setLots(nouveauLots);
      })
      .catch(error => {});
  }

  return (
    <div>
      <LotList lots={lots} ajoutLotOnClick={ajoutLotOnClick} />
    </div>
  );
};

const mapStateToProps = state => {
  console.log("store: ", state);
  return {
    idep: state.generalReducer.keycloak.tokenParsed.preferred_username
  };
};
export default connect(mapStateToProps)(LotListContainer);
