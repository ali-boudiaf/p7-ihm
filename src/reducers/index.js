import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { generalReducer } from "./generalReducer";
import ReducerLots from "./reducer-lots";
import ReducerActiveLot from "./reducer-active-lot";

const combinedReducer = combineReducers({
  activeLot: ReducerActiveLot,
  lots: ReducerLots,
  routing: routerReducer,
  generalReducer
});

export default combinedReducer;
