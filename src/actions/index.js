import axios from "axios";
import { AT_LOTS } from "./action-types";
const END_POINT = "http://localhost:9090/";
const LOTS_REPRISE_Q = "lots-reprise-q/idep/";
const LOTS_REPRISE_Q_BY_ID = "lots-reprise-q/id/";
const LOTS_REPRISE_Q_PRENDRE = "lots-reprise-q/nonaffecte/";

export function readAllLot(idep) {
  return function(dispatch) {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("", response);
        console.log("-------------");
        dispatch({ type: AT_LOTS.READ_ALL, payload: response.data });
      })
      .catch(error => {
        dispatch({ type: AT_LOTS.ERROR, payload: error.data });
      });
  };
}

export function affecterLot(idep) {
  return function(dispatch) {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_PRENDRE}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("", response);
        console.log("-------------");
        dispatch({ type: AT_LOTS.CREATE_POST, payload: response.data });
      })
      .catch(error => {
        dispatch({ type: AT_LOTS.ERROR, payload: error.data });
      });
  };
}

export function readLot(id) {
  return function(dispatch) {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_BY_ID}${id}`)
      .then(response => {
        console.log("resultat de la requête", response.data);
        dispatch({ type: AT_LOTS.READ, payload: response.data });
      })
      .catch(error => {
        dispatch({ type: AT_LOTS.ERROR, payload: error.data });
      });
  };
}
