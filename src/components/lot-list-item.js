import React from "react";
import { Link } from "react-router-dom";

const LotListItem = props => {
  const { lot } = props;
  return (
    <tr>
      <td>
        {/* {lot.lotId} */}
        <Link to={`lot/${lot.lotId}`}>{lot.lotId}</Link>
      </td>
      <td>
        {/* <button className="btn btn-danger">Supprimer</button> */}
        {lot.idep}
      </td>
    </tr>
  );
};

export default LotListItem;
