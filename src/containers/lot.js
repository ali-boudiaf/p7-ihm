import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { readLot } from "../actions/index";
import LotContent from "../components/lot-content";
import { Link } from "react-router-dom";

class Lot extends Component {
  componentWillMount() {
    // read de param
    // match.params.id
    console.log("Props du componant Lot: ", this.props);
    this.props.readLot(this.props.match.params.id);
  }

  renderLotContent() {
    const { lot } = this.props;
    if (lot) {
      return <LotContent lot={lot} />;
    }
  }

  render() {
    console.log("Résultat du mapStateToPros: ", this.props.lot);
    return (
      <div>
        {this.renderLotContent()}
        <Link className="button_space" to="/">
          {" "}
          <button className="btn btn-link">Retour</button>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    lot: state.activeLot
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ readLot }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lot);
