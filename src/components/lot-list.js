import React, { Component } from "react";
import LotListItem from "./lot-list-item";

export default class LotList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lots: this.props.lots
    };
  }

  componentDidUpdate() {
    console.log("je me met à jour :", this.props);
  }
  renderLots() {
    const { lots } = this.props;
    if (lots) {
      return lots.map(lot => {
        return <LotListItem key={lot.lotId} lot={lot} />;
      });
    }
  }

  render() {
    console.log("état du state: ", this.state);
    return (
      <div className="default_margin_top">
        <h1>Mes lots</h1>
        <div className="button_add">
          <button
            className="btn btn-primary btn-circle btn-lg "
            onClick={this.props.ajoutLotOnClick}
          >
            +
          </button>
        </div>
        <table className="table table-hover">
          <thead>
            <tr>
              <th>ID lot</th>
              <th>Idep lot</th>
            </tr>
          </thead>
          <tbody>{this.renderLots()}</tbody>
        </table>
      </div>
    );
  }
}
