import React, { Fragment } from "react";
import MaterialTable from "material-table";
import { Redirect } from "react-router";

import MonComposantModal from "./MonComposantModal";

export default function MaterialTableDemo() {
  // States
  const [open, setOpen] = React.useState(false);

  const [maData, setMaData] = React.useState("");

  const [redir, setRedir] = React.useState(false);

  const [lotId, setLotId] = React.useState("");

  const [titreDuLot, setTitreDuLot] = React.useState("Détails du lot ");

  const handleOpen = param => {
    setMaData(param);
    setOpen(true);
    setTitreDuLot("Détails du lot " + param.lotId);
  };
  const handleClose = () => {
    setOpen(false);
    setMaData("");
    setTitreDuLot("");
  };
  const redirigerVersLot = param => {
    setRedir(true);
    setLotId(param.lotId);
  };

  const [donnees, setDonnees] = React.useState({
    columns: [
      { title: "N° lot", field: "lotId", type: "numeric" },
      { title: "BI", field: "nbBi", type: "numeric" },
      { title: "BI en attente", field: "nbBiAtt", type: "numeric" },
      { title: "BI traité", field: "nbBiTraite", type: "numeric" },

      { title: "BI Activité", field: "nbBiRepAct", type: "numeric" },
      {
        title: "BI Activité en attente",
        field: "nbBiRepActAtt",
        type: "numeric"
      },
      {
        title: "BI Activité traité",
        field: "nbBiRepActTraite",
        type: "numeric"
      },

      { title: "BI Profession", field: "nbBiRepProf", type: "numeric" },
      {
        title: "BI Profession en attente",
        field: "nbBiRepProfAtt",
        type: "numeric"
      },
      {
        title: "BI Profession traité",
        field: "nbBiRepProfTraite",
        type: "numeric"
      }
    ],
    data: [
      {
        lotId: 44446,
        idep: "toto",
        lotDate: null,
        lotRang: "1",
        lotPriorite: null,
        modeTraitement: null,
        lotStatut: "N",
        nbBi: 40,
        nbBiAtt: 0,
        nbBiRepAct: 40,
        nbBiRepActAtt: 0,
        nbBiRepActTraite: 0,
        nbBiTraite: 0,
        depLot: null,
        nbBiRepProf: 0,
        nbBiRepProfAtt: 0,
        nbBiRepProfTraite: 0,
        vagueId: null,
        numLs: null,
        typeLot: "A",
        origineLot: "Q"
      },
      {
        lotId: 44447,
        idep: "toto",
        lotDate: null,
        lotRang: "2",
        lotPriorite: null,
        modeTraitement: null,
        lotStatut: "N",
        nbBi: 40,
        nbBiAtt: 0,
        nbBiRepAct: 40,
        nbBiRepActAtt: 0,
        nbBiRepActTraite: 0,
        nbBiTraite: 0,
        depLot: null,
        nbBiRepProf: 0,
        nbBiRepProfAtt: 0,
        nbBiRepProfTraite: 0,
        vagueId: null,
        numLs: null,
        typeLot: "A",
        origineLot: "Q"
      }
    ]
  });

  if (redir) {
    return (
      <>
        <Redirect push to={`/lot/${lotId}`} />
      </>
    );
  } else {
    return (
      <Fragment>
        <MaterialTable
          actions={[
            {
              icon: "launch",
              tooltip: "Ouvrir le lot",
              onClick: (event, rowData) => redirigerVersLot(rowData)
            },
            {
              icon: "search",
              tooltip: "Détails du lot",
              onClick: (event, rowData) => handleOpen(rowData)
            }
          ]}
          options={{
            search: false
          }}
          title="Mes lots"
          columns={donnees.columns}
          data={donnees.data}
          components={{
            Pagination: () => <></>
          }}
        />
        <MonComposantModal
          maData={maData}
          setMaData={setMaData}
          open={open}
          setOpen={setOpen}
          handleClose={handleClose}
          titre={titreDuLot}
        />
      </Fragment>
    );
  }
}
