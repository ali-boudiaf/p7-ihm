import React from "react";

const LotContent = ({ lot }) => {
  console.log("Lot reçu en props: ", lot);
  console.log("individus", lot.individusQualite);
  if (!lot) {
    return <div>Pas de lot trouvé</div>;
  }
  const renderIndividusQualites = () => {
    if (lot.individusQualite) {
      return lot.individusQualite.map((unIndividusQualite, i) => {
        return <div key={i}>Un individusQualite n°{i}</div>;
      });
    }
  };
  return (
    <div>
      <h2>{lot.lotId}</h2>
      <p>Informations du lot</p>
      <div>{renderIndividusQualites()}</div>
    </div>
  );
};

export default LotContent;
