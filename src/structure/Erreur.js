import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { clearErreur } from "../reducers/generalReducer";
import { Dialog, DialogTitle, DialogActions, Button } from "@material-ui/core";

class Erreur extends Component {
  handleClose = () => {
    const { clearErreur } = this.props;
    clearErreur();
  };

  render() {
    const { erreurs } = this.props;
    const mapErreurs = erreurs.map(erreur => {
      switch (erreur.error) {
        case "Error: Network Error":
          return "Le serveur ne répond pas";
        case "Error: Request failed with status code 500":
          return "Il y a eu un problème avec le serveur";
        default:
          return erreur.log ? erreur.log : erreur.error;
      }
    });
    return (
      <Dialog
        open={mapErreurs.length > 0}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle>{mapErreurs.length > 0 ? mapErreurs[0] : ""}</DialogTitle>
        <DialogActions>
          <Button variant="contained" onClick={this.handleClose} color="secondary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ generalReducer }) => {
  return { erreurs: generalReducer.erreurs };
};

const mapDispatchToProps = dispatch => bindActionCreators({ clearErreur }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Erreur);
