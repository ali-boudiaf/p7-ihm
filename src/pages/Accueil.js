import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core";
import Template from "../components/Template";
import LotList from "../components/lot-list";

const Accueil = ({ classes }) => {
  return (
    <Fragment>
      <Template />
      {/* <LotList /> */}
    </Fragment>
  );
};

const styles = {
  divider: {
    marginTop: 20,
    marginBottom: 20,
    width: "50%"
  }
};

export default withStyles(styles)(Accueil);
